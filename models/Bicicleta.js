var mongoose = require('mongoose');
var schema = mongoose.schema;

var bicicletaSchema = new mongoose.Schema({
    code: Number,
    modelo: String,
    color: String,
    ubicacion: {
        type: [Number], index : { type: '2dsphere', sparce: true}
    }
});

bicicletaSchema.methods.toString = function(){
    return 'code: ' + this.code + ' color: ' + this.color
};

bicicletaSchema.statics.allBicis = function(cb){
    return this.find({}, cb);
};

bicicletaSchema.statics.add = function(nbici, cb){
    this.create(nbici, cb);
}

bicicletaSchema.statics.findById = function(aCode, cb){
    return this.findOne({code: aCode}, cb);
};

bicicletaSchema.statics.removeById = function(aCode, cb){
    return this.deleteMany({code: aCode}, cb);
};

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion,
    })
};

module.exports = mongoose.model('Bicicleta', bicicletaSchema);

/*
var Bicicleta = function (id, modelo, color, ubicacion) {
    this.id = id;
    this.modelo = modelo;
    this.color = color;
    this.ubicacion = ubicacion;
}

Bicicleta.prototype.toString = function(){
    return this.modelo + 'color' + this.color;
}

//esto se almacenara en una base de datos en el futuro
Bicicleta.allBicis = [];
Bicicleta.add = function (aBici){
    this.allBicis.push(aBici);
}

Bicicleta.getUbiBici = function(index){
    return this.allBicis[index].ubicacion;
}

Bicicleta.findById = function(biciId){
    var unabici = this.allBicis.find(x => x.id == biciId);
    if(unabici)
        return unabici;
    else 
        throw new Error("No existe bicicleta con id " + unabici.id); 
}

Bicicleta.ExistsById = function(biciId){//para implementar save
    var unabici = this.allBicis.find(x => x.id == biciId);
    if(unabici)
        return unabici;
    else 
        return null; 
}

Bicicleta.removeById = function(biciId){
    var unabici = this.findById(biciId);
    for(var i = 0; i <= this.allBicis.length; i++){
        if (this.allBicis[i].id == biciId){
            this.allBicis.splice(i, 1);
            break;
        }
    }
}

/*
var b1 = new Bicicleta(0, 'Playa', 'rojo', [-34.916079, -56.173660]);
var b2 = new Krill(1, 'Ruta', 'azul', [-34.913883, -56.218783]);


Bicicleta.add(b1);
Bicicleta.add(b2);


module.exports = Bicicleta;
*/