var mongoose = require('mongoose');
var schema = mongoose.schema;
var reserva = require('./Reserva');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const mailer = require('/mailer/mailer')
const token = require('/Token');
const uniqueValidator = require('mongoose-unique-validator');
const saltRounds = 10;

const validateEmail = function(email){
    const regex = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return regex.test(email);
};

var usuarioSchema = new mongoose.Schema({
    nombre: {
        type: String,
        trim : true,
        required: [true, 'campo obligatorio']
    },
    email: {
        type: String,
        trim: true,
        unique: true,
        required: [true, 'campo obligatorio'],
        lowercase: true,
        validate: [validateEmail, 'email invalido'],
        match: /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
    },
    password: {
        type: String,
        required: [true, 'campo obligatorio'],
    },
    passwordResetToken: true,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false,
    }
});

usuarioSchema.plugin(uniqueValidator, {message: '(PATH) ya tiene un usuario registrado'});

usuarioSchema.statics.allUsuarios = function(cb){
    return this.find({}, cb);
}

usuarioSchema.statics.add = function(nusuario, cb){
    this.create(nusuario, cb);
}

usuarioSchema.pre('save', (next) =>{
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.enviar_mail_bienvenida = function(cb){
    token = new Token({_userId:this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err){return console.log(err.message);}

        const mailOptions = {
            from: 'no-reply@redbicicetas.com',
            to: email_destination,
            subject: 'verificacion de cuenta',
            text: 'hola, \n\n' + 'Para verificar su cuenta haga click en este link \n' + `http://localhost:3000/usuarios/chachacha/verificar`
        };

        mailer.sendMail(mailOptions, function(err){
            if(err){return console.log(err.message);}

            console.log('A verification email has been sent to ' + email_destination);
        });
    });
}

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync(this.password, password);
}

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);
}

module.exports = mongoose.model('Usuario', usuarioSchema);