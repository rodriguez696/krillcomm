var express = require('express');
var router = express.router();
var controller = require('../controllers/token');

router.get('/confirmation/:token', controller.confirmationGet);

module.exports = router;