var express = require('express');
var controller = require('../controllers/usuario');
var router = express.Router();

/* GET users listing. */
router.get('/', controller.list);
router.get('/create', controller.create_get);
router.post('/create', controller.creat_post);
router.get('/update', controller.list);

module.exports = router;
