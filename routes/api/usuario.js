var express = require('express');
var router = express.Router();
var controller = require('../../controllers/api/usuarioControllerAPI');

router.post('/reservar', controller.reservar);

module.exports = router;