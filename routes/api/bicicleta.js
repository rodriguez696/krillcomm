var express = require('express');
var router = express.Router();
var controller = require('../../controllers/api/bicicletaControllerAPI');

router.get('/', controller.bici_list);
router.post('/create', controller.bici_create);
router.post('/delete', controller.bici_delete);
router.post('/update', controller.bici_update);
//save: guarda krill nuevo o actualiza uno existente
//router.post('/save', controller.bici_save);

module.exports = router;