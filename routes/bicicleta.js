var express = require('express');
var router = express.Router();
var controller = require('../controllers/bicicleta');

router.get('/', controller.bici_list);
router.get('/create', controller.bici_create_get);
router.post('/create', controller.bici_create_post);
router.post('/delete', controller.bici_delete_post);
router.get('/:id/update', controller.bici_update_get);
router.post('/:id/update', controller.bici_update_post);

module.exports = router;