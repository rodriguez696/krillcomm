const passport = require('passport');
const localStratey = require('passport-local').Strategy;
const usuario = require('../models/Usuario');

passport.use(new localStrategy( function(email, password, done){
    usuario.findOne({email:email}, function(err, usr){
        if(err) return done(err);
        if(!user) return doe(null, false, {message: 'no existe usuario registrado con este mail'});
        if(!usr.validPassword(password)) return done(null, false,{message: 'contraseña invalida'});

        return done(null, usr);
    })
}));

passport.serializeUser(function(user, cb){
    cb(null, user.id);
});

passport.deserializeUser(function(id, cb){
    Usuario.findById(id, function(err, usuario){
        cb(err, usuario);
    });
});

module.exports = passport;