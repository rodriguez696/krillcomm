var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/usuario');
var bicicletaRouter = require('./routes/bicicleta');
var bicicletaAPIRouter = require('./routes/api/bicicleta');
var authAPIRouter = require('./routes/api/auth');
var usuarioAPIRouter = require('./routes/api/usuario');
var usuarioRouter = require('./routes/usuario');
var tokenRouter = require('./routes/token');
const passport = require('./config/passport');
const session = require('express-session');
const jwt = require('jasonwebtoken');

const store = session.MemoryStore;

var app = express();

app.set('secretKey', 'jwt_pwd_11223344');

app.use(session({
  cookie:{maxAge: 240 + 60 + 60 + 1000},
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis_cualquier_cosa'
}));

var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost/red_bicicletas';
mongoose.connect(mongoDB, {useNewUrlParser: true});
mongoose.Promise = global.Promise;
var db = mongoose.connection;
db.on('Error', console.error.bind(console, 'mongoDB connection error: '));


// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.get('/login', function(req, res){
  res.render('session/login');
});

app.post('/login', function(req, res, next){
  passport.authenticate('local', function(err, user, info){
    if(err) return next(err);
    if(!usuario) return res.render('session/login', {info});
    req.logIn(usuario, function(err){
      if(err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res){
  req.logOut();
  res.redirect('/');
});

app.get('/forgoPassword', function(req, res){

});

app.post('/forgotPassword', function(req, res){

});

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/bicicletas', loggedIn, bicicletaRouter);

app.use('/api/auth', authAPIRouter);
app.use('/api/bicicletas', validarUsuario, bicicletaAPIRouter);
app.use('/api/reservas', usuarioAPIRouter);
app.use('/usuarios', usuarioRouter);
app.use('/tokens', tokenRouter);
app.use(passport.initialize());
app.use(passport.session());
// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next){
  if(req.user){
    next();
  }else{
    console.log('user no esta logueado');
    res.redirect('/login');
  }
};

function validarUsuario(req, res, next){
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded){
    if(err){
      res.json({status: 'error', message: err.message, data:null});
    }else{
      req.body.userId = decoded.id;
      console.log('jwt verify: ' + decoded);
      next();
    }
  });
}



module.exports = app;
