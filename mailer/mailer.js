const nodemailer = require('nodemailer');

const mailConfig = {
    host: 'smtp.ethereal.mail',
    port: 587,
    auth: {
        user: 'algoalgoalgo@ethereal.email',
        pass: 'algoalgoalgo'
    }
};

module.exports = nodemailer.createTransport(mailConfig);