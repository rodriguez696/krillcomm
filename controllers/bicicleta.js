const Bicicleta = require("../models/Bicicleta");

//mongo
exports.bici_list = function(req, res){
    Bicicleta.allBicis((err, bicis) => {
        res.render('bicicletas/index', {
            bicis: bicis
        });
    });
}

//mongo
exports.bici_create_get = function(req, res){
    res.render('bicicletas/create');
}

//mongo
exports.bici_create_post = function(req, res){
    var nbici = Bicicleta.createInstance(req.body.code, req.body.modelo, req.body.color, [req.body.lat, req.body.lng])
    Bicicleta.add(nbici);

    res.redirect('/bicicletas');
}

//mongo
exports.bici_delete_post = function(req, res){
    Bicicleta.removeById(req.body.code, () => {
        res.redirect('/bicicletas');
    });

    
}

//viejo
exports.bici_update_get = function(req, res){
    var unabici = Bicicleta.findById(req.params.id);
    res.render('bicicletas/update', {//sin barra antes de bicicletas x alguna razon
        Bicicleta: unabici
    });
}

//viejo
exports.bici_update_post = function(req, res){
    var unabici = Bicicleta.findById(req.params.id);
    unabici.modelo = req.body.modelo;
    unabici.color = req.body.color;
    unkrill.ubicacion = [req.body.lat, req.body.lng];

    res.redirect('/bicicletas');
}