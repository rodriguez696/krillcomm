const Usuario = require("../models/Usuario");

exports.list = function(req, res, next){
   Usuario.find({}, (err, usuarios) => {
       res.render('usuarios/index', {usuarios:usuarios});
   });
}


exports.create_get = function(req, res, next){
    res.render('usuarios/create', {errors: {}, usuario: new Usuario()});
}

exports.create_post = function(req, res, next){
    if(req.body.password != req.body.confirm_password){
        res.rener('usuarios/create', {errors: {confirm_password: {message:'passwords no coinciden'}}, usuario: new Usuario()});
        return;
    }

    Usuario.create({nombre:req.body.nombre, email:req.body.email, password:req.body.password}, function(err, nuevoUsuario){
        if (err) {
            res.render('usuarios/create', {errors: err.errors, usuario: new Usuario()});
        }else{
            nuevoUsuario.enviar_mail_bienvenida();
            res.redirect('/usuarios');
        }
    })
}

//falta el update x el momento
//tambien el delete