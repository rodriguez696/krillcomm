const Bicicleta = require("../../models/Bicicleta");

exports.bici_list = function(req, res){
    Bicicleta.find({}, (err, bicis) => {
        res.status(200).json({
            bicicletas: bicis
        })
    })
};

exports.bici_create = function(req, res){

    const bici = Bicicleta.createInstance(req.body.id, req.body.color, req.body.modelo, [req.body.lat, req.body.lng]);
    Bicicleta.add(bici, () => {
        console.log("bici agregada");
    });
    
    res.status(200).json({
        bicicleta: bici
    });

};

exports.bici_delete = function(req, res){
    console.log('borrar id: ' + req.body.id);
    Bicicleta.removeById(req.body.id, () => {
        res.status(200).json({
            "text":"exito"
        });
    });

    
}

//esto no funciona de momento 
exports.bici_update = function(req, res){
    var unabici = Bicicleta.findById(req.body.id);
    unabici.modelo = req.body.modelo;
    unabici.color = req.body.color;
    unabici.ubicacion = [req.body.lat, req.body.lng];

    res.status(200).json({
        unabici
    });
}

/*
exports.bici_save = function(req, res){
    var unabici = Bicicleta.ExistsById(req.body.id);
    if (unabici){
        exports.bici_update(req, res);
    }else{
        exports.bici_create(req, res);
    }
}*/