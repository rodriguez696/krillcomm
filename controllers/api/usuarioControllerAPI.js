const Usuario = require('../../models/Usuario');

exports.reservar = function(req, res){
    Usuario.reservar(req.biciId, req.desde, req.hasta, (err, reserva) => {
        if (err) console.log(err);
        res.status(200).json({
            reserva
        });
    });
};