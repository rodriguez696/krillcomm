var mongoose = require('mongoose');
var Bicicleta = require('../../models/Bicicleta');
var Usuario = require('../../models/Usuario');
var Reserva = require('../../models/Reserva');

describe('testing usuarios', function(){
    beforeEach(function(done) {
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('eror', console.error.bind(console, 'connection error'));
        db.once('Open', function(){
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Reserva.deleteMany({}, function(err, success){
            if (err) console.log(err);
            Usuario.deleteMany({}, function(err, success){
                if (err) console.log(err);
                Bicicleta.deleteMany({}, function(err, success){
                    if (err) console.log(err);
                    done();
                });
            });
        });

        describe('cuando un usuario reserve una bicicleta', () => {
            it ('desde existir una reserva', (done) => {
                const usuario = new Usuario({nombre: 'Pedro'});
                usuario.save();
                const bicicleta = new Bicicleta({code: 1, color: 'verde', modelo: 'ruta'});
                bicicleta.save();

                var hoy = new Date();
                var maniana = new Date();
                maniana.setDate(hoy.getDate + 1);
                usuario.reservar(bicicleta.id, hoy, maniana, function(err, reserva){
                    Reserva.find({}).populate('bicicleta').populate('usuario').exec(function(){
                        console.log(reservas[0]);
                        expect(reservas.length).toBe(1);
                        expect(reservas[0].diasDeReserva()).toBe(2);
                        expect(reservas[0].bicicleta.code).toBe(1);
                        expect(reservas[0].usuario.nombre).toBe(usuario.nombre);
                        done();
                    });
                });
            }) ;
        });
    });
});