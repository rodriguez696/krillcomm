var mongoose = require('mongoose');
const Bicicleta = require('../../models/Bicicleta');
var bicicleta = require('../../models/Bicicleta');

describe('Testing bicicletas', function(){
    beforeEach(function(done){
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('bicicleta.createinstance', () => {
        it('crea una instancia de bicicleta', () => {
            var bici = Bicicleta.createInstance(1, 'verde', 'playa', [-34, -52]);

            expect(bici.code).toBe(1);
        });
    });

    describe('bicicleta.allbicis', () => {
        it('comienza vacia', (done) => {
            Bicicleta.allBicis(function (err, bicis) {
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('bicicleta.add', () => {
        it('agrega una sola bici', (done) => {
            var bici = new Bicicleta({code: 1, color:'verde', modelo:'playa', ubicacion:[-34, -52]})
            Bicicleta.add(bici, function(err, nbici){
                if (err) console.log(err);
                Bicicleta.allBicis(function (err, bicis) {
                    expect(bicis.length).toBe(1);
                    expect(bicis[0].code).toEqual(1);
                    done();
                });
            });
        });
    });
});



/*
beforeEach(() => {
    bicicleta.allBicis = [];
});

describe(bicicleta.allBicis, () => {
    it('comienza vacia', () => {
        expect(bicicleta.allBicis.length).toBe(0);
    });
});

describe(bicicleta.add, () => {
    it('agregamos una', () => {
        expect(bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(0,"aaa", "aaa", null);
        bicicleta.add(a);
        expect(bicicleta.allBicis.length).toBe(1);
        expect(bicicleta.allBicis[0]).toBe(a);
    });
});

describe(bicicleta.findById, () => {
    it('debe devolver la bici con id 1', () => {
        expect(bicicleta.allBicis.length).toBe(0);
        var a = new Bicicleta(5,"aaa", "aaa", null);
        var b = new Bicicleta(1,"aaas", "asa", null);
        bicicleta.add(a);
        bicicleta.add(b);
        var targetbici = bicicleta.findById(1);
        expect(targetbici.id).toBe(1);
    });
})*/