const Bicicleta = require('../../models/Bicicleta');
var bicicleta = require('../../models/Bicicleta');
var mongoose = require('mongoose');
var express = require('express');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas'

describe('bicicleta API', () => {
    beforeAll(() => { 
        var mongoDB = 'mongodb://localhost/testdb';
        mongoose.connect(mongoDB, {useNewUrlParser: true});
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('we are connected to test database');
            done();
        });
    });
    
    afterAll((done) => {
        mongoose.disconnect();
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });

    describe('GET BICICLETAS', () => {
        it('status 200', () => {
            expect(bicicleta.allBicis.length).toBe(1);

            request.get(base_url, function(error, response, body){
                expect(response.statusCode).toBe(200);
            });
        });
    });

    describe('POST BICICLETAS CREATE', () => {
        it('status 200', (done) => {
            expect(bicicleta.allBicis.length).toBe(1);
            var header = {'content-type':'application/json'}
            var abici = '{ "id" : 10, "color" : "rojo", "modelo" : "playa", "lat" : -34, "lng" : -54 }';

            post_url = base_url + '/create'

            request.post({
                headers: header,
                url: post_url,
                body: abici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                expect(bicicleta.findById(10).id).toBe(10);
                done();
            });
        });
    });
  
});